

  
(function ($, Drupal) {

  
    Drupal.behaviors.triggerFlag = {
      attach: function(context, settings) {
        // TODO: Move to only flagged pages
        setTimeout(function(){
          if($('.flag__leido--off a').length > 0) {
            $('.flag__leido--off a').trigger('click');
          }
        }, 10000)
  
        var counter = 0;
        $('.flag__leido').on('click', function(){
          if(counter >=4) {
            $(this).addClass('flag__leido--trick')
          }
          counter++;
        })
      }
    }
  
    Drupal.behaviors.openComment = {
      attach: function(context, settings) {
        $('.comment-add').next('.comment-comment-form').hide();
        $('.comment-add').on('click', function(){
            $(this).next('.comment-comment-form').toggleClass('comment-form--active');
        })
      }
    }
  
    Drupal.behaviors.tourLink = {
      attach: function(context, settings) {
        $('.tour-link').on('click', function(e) {
          var modal = confirm('Activar la visita guiada borrará todo el contenido que no haya guardado. ¿Desea continuar?');
          if(!modal) {
            e.preventDefault();
          }
        });
      }
    }

  
    // Drupal.behaviors.fotogaleriaHover = {
    //   attach: function(context, settings) {
    //     $('.slick__item a').on('click', function(e){
    //       if(!$('html').hasClass('touchevents')) {
    //         return;
    //       }
  
    //       if(!$(this).parent('.slick__item').hasClass('js-hover')) {
    //         e.preventDefault();
    //         $('.slick__item').removeClass('js-hover');
    //         $(this).parent('.slick__item').addClass('js-hover');
    //         return;
    //       }
  
    //     });
    //   }
    // }
    
  
})(jQuery, Drupal);