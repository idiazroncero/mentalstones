

  
(function ($, Drupal) {
  
  Drupal.behaviors.hackPM = {
    attach: function(context, settings) {
      var user = $('.pm-username').attr('data-pm-username');
      if(user) {
        $('.main-title h1').once('hacktitle').text(Drupal.t('Start conversation with') + ' ' + user);
        $('#thread-members-display-container').hide();
      }
      //$('#thread-members-display-container label').hide();
      //$('#thread-members-input').hide();
    }
  }
    
  
})(jQuery, Drupal);