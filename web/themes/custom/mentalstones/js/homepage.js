(function ($, Drupal) {


  var transition_time = 330; //Y Keep in sync with _confiog.scss

  Drupal.behaviors.closeMsg = {
    attach: function(context, settings) {
      $('.message__close').on('click', function(){
        $(this).parent().fadeOut();
      })
    }
  }

  Drupal.behaviors.cardHover = {
    attach: function(context, settings) {
      $('.contenido__link-layer').on('click', function(e){

        if(!$('html').hasClass('touchevents')) {
          return;
        }

        if(
          !$(this).parents('.contenido').hasClass('js-hover') &&
          $(this).parents('.views-row-first').length == 0
        ) {
          e.preventDefault();
          $('.contenido--teaser, .contenido--mensaje').removeClass('js-hover');
          $('.contenido__click').remove();
          $(this).after('<div class="contenido__click">' + Drupal.t('Press to open') + '</div>');
          $(this).parents('.contenido--teaser').addClass('js-hover');
          return;
        }

      });

      $('.contenido--mensaje').on('click', function(e){

        if(!$('html').hasClass('touchevents')) {
          return;
        }

        if(!$(this).hasClass('js-hover')) {
          e.preventDefault();
          $('.contenido__click').remove();
          $('.contenido--teaser, .contenido--mensaje').removeClass('js-hover');
          $(this).addClass('js-hover');
          return;
        }

      });
    }
  }

  Drupal.behaviors.randomColor = {
    attach: function(context, settings) {
      function getContrast(randomColor){
        var realColor = randomColor.replace('#', '');
        var contrast = parseInt(realColor, 16) > 0xffffff/1.75 ? 'black':'white';
        return contrast;
      }

      $('.js-randomcolor').each(function(){
        var colors = $.trim($(this).attr('data-colors')).split('/');
        var random = Math.floor(Math.random() * colors.length );
        var randomColor = colors[random];
      
        // Set the bg
        $(this).css('background-color', randomColor);
        
        // And update text color accordingly
        var contrast = getContrast(randomColor);
        if(contrast == 'white') {
          $(this).addClass('contenido__title--inverse');
        }
      });
    }
  }

  Drupal.behaviors.randomPopCard = {
    attach: function(context, settings) {
      var $cards = $('.contenido--teaser');
      var mousemove = false;
      var interval;

      $(document).on('mousemove', function(){
        mousemove = true;
      });

      function activateInterval() {
        interval = setInterval(function(){ 
          var random = Math.floor(Math.random() * $cards.length);
          $cards.eq(random).addClass('auto-hover');
          setTimeout(function(){
            $cards.eq(random).removeClass('auto-hover');
          },3000)
        }, 6000);
      };

      setInterval(function(){
        if(mousemove == true) {
          if(interval) {
            console.log('interval desactivado');
            clearInterval(interval);
            interval = undefined;
            $cards.removeClass('auto-hover');
          }
        } else {
          if(!interval) {
            activateInterval();
          }
        }
        mousemove = false;
      }, 1000);
    }
  }

  // Drupal.behaviors.tiltCard = {
  //   attach: function(context, settings) {
  //     $('.contenido__fit').tilt({
  //       perspective: 100, 
  //       scale: 1.6,
  //     })
  //   }
  // }

  

})(jQuery, Drupal);