(function ($, Drupal) {
  
    Drupal.behaviors.validateSoundcloud = {
      attach: function (context, settings) {
        // Hide field
        var globalErrors = 0;
        function errorTemplate(text){
          var template = '<div class="message" role="contentinfo" aria-label="Mensaje de error">';
          template+= '<div class="message__text message--error" role="alert">';
          template += '<h2 class="visually-hidden">Mensaje de error</h2>';
          template += text;
          template += '</div></div>';
          return template;
        }

        $('.field--name-field-soundcloud-url .form-text').on('blur', function(){
          var that = $(this);
          that.siblings('.message').remove();
          globalErrors--;
          $.ajax({
            async: false,
            method: "GET",
            url: "https://soundcloud.com/oembed?format=json&url=" + $(this).val(),
            error: function(){
              var alert = errorTemplate('¡Ojo! Revisa la URL que has introducido: no parece correcta');
              that.before(alert);
              globalErrors++;
            }
          })
        });

        $('form#node-proyecto-edit-form, form#node-proyecto-form').once('validateSoundcloud').on('submit', function(e){
          var soundcloudUrls = $('.field--name-field-soundcloud-url .form-text');

          soundcloudUrls.each(function(){
            $.ajax({
              async: false,
              method: "GET",
              url: "https://soundcloud.com/oembed?format=json&url=" + $(this).val(),
              error: function(){
                globalErrors++
              }
            })
          })

          console.log(globalErrors);

          if(globalErrors > 0) {
            e.preventDefault();
          } 
        })

      }
    };

  
})(jQuery, Drupal);