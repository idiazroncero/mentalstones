(function ($, Drupal) {


    Drupal.behaviors.toggleFigcaption = {
        attach: function (context, settings) {
            $('#edit-field-pie-de-foto-portada-wrapper').hide();
            $('#figcaption-toggler').on('click', function(e){
                e.preventDefault();
                $('#edit-field-pie-de-foto-portada-wrapper').show();
                $(this).hide();
            })

        }
    };

    Drupal.behaviors.createPreview = {
      attach: function (context, settings) {
        $('#do-preview').once('createpreview').on('click', function(e){
            $('.preview-item').remove();
            $('.preview .message, .preview h4').remove();

            e.preventDefault();
            var color = $('#edit-field-color-0-value').val() || '#000000';
            var title = $('#edit-title-0-value').val() || 'Inserte un título para verlo aquí';
            var subtitle = $('#edit-field-subtitulo-0-value').val() || false;
            var autor = $('#block-mentalstones-account-menu-menu').text();
            var image = $('.focal-point-wrapper img').attr('src') || 'https://placehold.it/1000x1000.jpg';
            var inverse = false;

            function getContrast(randomColor){
                var realColor = randomColor.replace('#', '');
                return (parseInt(realColor, 16) > 0xffffff/2) ? 'black':'white';
            }

            function messageTemplate(msg){
                var template = '<div class="message message__text message--error">';
                template += '<p>' + msg + '</p></div>'
                return template;
            }
      
            var contrastColor = getContrast(color);
            if(contrastColor == 'white') {
                inverse = true; 
            }

            function frontTemplate() {
                var clase = inverse ? ' contenido__title--inverse' : false;
                var template = '<p>PORTADA</p><div class="preview-item preview--front">'
                template += '<article class="contenido contenido--teaser">';
                template += '<div class="contenido__inner">';
                template += '<a href="/" class="contenido__link-layer"></a>';
                template += '<div class="contenido__fit">';
                template += '<img src="' + image + '" alt="Prueba" typeof="foaf:Image" height="800" width="800">';
                template += '</div>';
                template += '<div class="contenido__title js-randomcolor' + clase +'" style="background-color:' + color + ';">';
                template += '<h1><span>' + title + '</span></h1>';
                if(subtitle) {
                    template += '<p class="contenido__subtitulo">' + subtitle + '</p>';
                }
                template += '<p class="contenido__autor"><span><a title="Ver perfil del usuario." href="/" >' + autor + '</a></span></p>';
                template += '</div></div></article></div>';
                return template;
            }

            function teaserTemplate() {
                var clase = inverse ? ' teaser--inverse' : '';
                var template = '<p>RESUMEN PEQUEÑO</p><div class="preview-item preview--teaser">'
                template += '<article class="contenido contenido--proyecto teaser-estandar' + clase + '">';
                template += '<div class="teaser-estandar__bg" style="background-color:' + color + '"></div>';
                template += '<a href="/" class="link-layer"></a>';
                template += '<div class="teaser-estandar__img">';
                template += '<img src="' + image + '" alt="Prueba" typeof="foaf:Image" height="800" width="800">';
                template += '</div>';
                template += '<div class="teaser-estandar__contenido">';
                template += '<h1 class="teaser-estandar__title"><span>' + title + '</span></h1>';
                if(subtitle) {
                    template += '<p class="teaser-estandar__subtitulo">' + subtitle + '</p>';
                }
                template += '<div class="teaser-estandar__autor">Por ' + autor + '</div>';
                template += '</div></article></div>';
                return template;
            }

            function bigTeaserTemplate() {
                var clase = inverse ? ' teaser--inverse' : '';
                var template = '<p>RESUMEN GRANDE</p><div class="preview-item preview--teaser">'
                template += '<article class="contenido contenido--proyecto teaser-grande' + clase + '">';
                template += '<div class="teaser-grande__bg" style="background-color:' + color + '"></div>';
                template += '<a href="/" class="link-layer"></a>';
                template += '<div class="teaser-grande__img"><div class="preview-squarer">';
                template += '<img src="' + image + '" alt="Prueba" typeof="foaf:Image" height="800" width="800">';
                template += '</div></div>';
                template += '<div class="teaser-grande__contenido">';
                template += '<h1 class="teaser-grande__title"><span>' + title + '</span></h1>';
                if(subtitle) {
                    template += '<p class="teaser-grande__subtitulo">' + subtitle + '</p>';
                } 
                template += '<div class="teaser-grande__autor">Por ' + autor + '</div>';
                template += '<div class="teaser-grande__texto">En este lugar aparecerá la descripción del artículo especificada en el paso 5. Descripciones muy largas aparecerán recortadas. Es mejor ser lo más breves y concisos posible.</div>';
                template += '</div></article></div>';
                return template;
            }

            // Inserta
            $(this).after(frontTemplate());
            $(this).after(teaserTemplate());
            $(this).after(bigTeaserTemplate());

            // Errores o cosas a mejorar
            if($('#edit-field-color-0-value').val() == '') {
                $('#do-preview').after(messageTemplate('Rellena el campo de color para ver reflejado el resultado'));
            }

            if($('#edit-title-0-value').val() == '') {
                $('#do-preview').after(messageTemplate('Rellena el título para ver reflejado el resultado'));
            }

            if(!$('.focal-point-wrapper img').attr('src')) {
                $('#do-preview').after(messageTemplate('Carga una imagen de portada para ver el resultado'));
            }

        });

        


      }
    };
  
})(jQuery, Drupal);