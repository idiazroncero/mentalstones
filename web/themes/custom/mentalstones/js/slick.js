(function ($, Drupal) {
  
    Drupal.behaviors.slickCarousel = {
      attach: function (context, settings) {
        $('.slick').slickLightbox({
            // adaptiveHeight: true,
            // speed: 600
            itemSelector : ".slick__item a",
            shouldOpen: function(slick, slickItem, event){
              if(!$('html').hasClass('touchevents')) {
                // Si no es mobile, siempre se abre
                return true;
              } else {
                if(!$(slickItem).parent().hasClass('js-hover')) {
                  // Desmarca cualquier otro abierto.
                  $('.slick__item').removeClass('js-hover');
                  // Marca como abierto
                  $(slickItem).parent().addClass('js-hover');
                  return false;
                } else {
                  // Si tiene clase es que esta activo, y por tanto se puede abrir.
                  return true;
                }
              }
            }
        });
      }
    }

  
})(jQuery, Drupal);