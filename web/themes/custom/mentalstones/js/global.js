(function($,sr){

  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function (func, threshold, execAsap) {
      var timeout;

      return function debounced () {
          var obj = this, args = arguments;
          function delayed () {
              if (!execAsap)
                  func.apply(obj, args);
              timeout = null;
          };

          if (timeout)
              clearTimeout(timeout);
          else if (execAsap)
              func.apply(obj, args);

          timeout = setTimeout(delayed, threshold || 100);
      };
  }
  // smartresize 
  jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');


(function ($, Drupal) {

  Drupal.behaviors.openMenu = {
    attach: function(context, settings) {
      $('.menu__open, .logo__ms').on('click', function(e){
        console.log('click');
        e.preventDefault();
        $('.front__layer').toggleClass('front__layer--open');
        $('.page__aside').toggleClass('page__aside--open');
        $('.menu__open').toggleClass('menu__open--open');
        $('body').toggleClass('prevent-scroll');
      })
    }
  }

  
  Drupal.behaviors.notifications = {
    attach: function (context, settings) {
      // $('.private-message-page-link').on('click', function(e){
      //     e.preventDefault();
      //     $('.notifications').toggleClass('notifications--open');
      // });

      // $('.private-message__reply').on('click', function(){
      //   $(this).siblings('.private-message__form').toggleClass('private-message__form--active');
      // })

      $('.private-message__form .form-submit').on('click', function(e){
        var $form = $(this).parents('form');
        var $text = $form.find('textarea');
        console.log($form, $text);
        if ($text.val() === '') {
          e.preventDefault();
          alert('rellene el form');
        } else {
          location.reload();
        }
      });

    }
  };

  Drupal.behaviors.closeMsg = {
    attach: function(context, settings) {
      $('.message__close').on('click', function(){
        $(this).parent().fadeOut();
      })
    }
  }

  Drupal.behaviors.hideSidebar = {
    attach: function(context, settings) {
      $('.sidebar__hide').on('click', function(){
        $('.page__aside').toggleClass('page__aside--hidden');
        $('#main-wrapper').toggleClass('noaside');
        if($(this).text() == '←') {
          $(this).text('→');
        } else {
          $(this).text('←');
        };
      })
    }
  }

  Drupal.behaviors.openHelp = {
    attach: function(context, settings) {
      $('.description__icon', context).once('openHelp').each(function(){
        $(this).on('click', function(e){
          e.preventDefault();
          $(this).parent().toggleClass('description--open');
        });
      });
    }
  }

  Drupal.behaviors.teaserColor = {
    attach: function(context, settings) {
      function getContrast(randomColor){
        var realColor = randomColor.replace('#', '');
        return (parseInt(realColor, 16) > 0xffffff/2) ? 'black':'white';
      }

      $('.teaser-grande, .teaser-estandar').each(function(){
        var color = $(this).css('background-color');
        
        // And update text color accordingly
        var contrast = getContrast(color);
        if(contrast == 'white') {
          $(this).addClass('teaser--inverse');
        }
      });
    }
  }

  Drupal.behaviors.dialogSelect = {
    attach: function (context, settings) {
      
      $('#dialog').once('dialog').dialog({
          width:350,
          modal:true,
          autoOpen: false
      });

      $('a[href="/en/create"], a[href="/crear"]').click(function(e){
          e.preventDefault();
          $('#dialog').dialog('open');
      });
    }
  }

  Drupal.behaviors.interaccionesMobile = {
    attach: function (context, settings) {

      function updateInteracciones() {
        var width = $(window).width();
        if(width <= 960) {
          var intHeight = $('.interacciones').outerHeight();
          $('#main-wrapper').css('margin-bottom', intHeight);
        } else {
          $('#main-wrapper').css('margin-bottom', 0);
        }
      }

      $(window).smartresize(function(){
        updateInteracciones();
      }); 
      
      updateInteracciones();
      
    }
  }

})(jQuery, Drupal);