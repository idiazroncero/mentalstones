(function ($, Drupal) {
  
    Drupal.behaviors.tabs = {
      attach: function (context, settings) {
        $('#tabs').tabs();
      }
    }

  
})(jQuery, Drupal);