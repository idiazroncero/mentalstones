<?php

namespace Drupal\message_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Message type entities.
 */
interface MessageEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
