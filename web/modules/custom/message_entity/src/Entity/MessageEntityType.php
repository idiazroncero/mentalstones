<?php

namespace Drupal\message_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Message type entity.
 *
 * @ConfigEntityType(
 *   id = "message_entity_type",
 *   label = @Translation("Message type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\message_entity\MessageEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\message_entity\Form\MessageEntityTypeForm",
 *       "edit" = "Drupal\message_entity\Form\MessageEntityTypeForm",
 *       "delete" = "Drupal\message_entity\Form\MessageEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\message_entity\MessageEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "message_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "message_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/message_entity_type/{message_entity_type}",
 *     "add-form" = "/admin/structure/message_entity_type/add",
 *     "edit-form" = "/admin/structure/message_entity_type/{message_entity_type}/edit",
 *     "delete-form" = "/admin/structure/message_entity_type/{message_entity_type}/delete",
 *     "collection" = "/admin/structure/message_entity_type"
 *   }
 * )
 */
class MessageEntityType extends ConfigEntityBundleBase implements MessageEntityTypeInterface {

  /**
   * The Message type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Message type label.
   *
   * @var string
   */
  protected $label;

}
