<?php

namespace Drupal\message_entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Message entities.
 *
 * @ingroup message_entity
 */
class MessageEntityDeleteForm extends ContentEntityDeleteForm {


}
